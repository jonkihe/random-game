import { flatten } from '@angular/compiler';
import { Component, OnInit, Input,ViewChild } from '@angular/core';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {
  player1: number;
  player2: number;
  randomNumer: number;
  winner: string;
  
  ngOnInit(): void {
   
  }

  spin(): void {
    if (this.player1 == null || this.player2 == null) {
      return;
    }
    this.randomNumer =  Math.floor(Math.random() * 101);
    this.winner = this.determTheWinner();
  }

  testIfNumber(event){
    if (isNaN(event.key))
      return false;
    if (!this.checkIfNumberInRange(event))
      return false;
    return true;
  }

  checkIfNumberInRange(event){
    try {
      if (event.path[1].classList[1] == 'one' && 
      this.checkIfNumberIsHigerThanHundrad(event.key, this.player1)){
        return false;
      }
      if (event.path[1].classList[1] == 'two'  && 
      this.checkIfNumberIsHigerThanHundrad(event.key, this.player2)){
        return false;
      }
      
    }
    catch {
      // this is hack around mozila firefox.
      if (event.srcElement.offsetParent.className == 'player one' && 
      this.checkIfNumberIsHigerThanHundrad(event.key, this.player1)){
        return false;
      }
      if (event.srcElement.offsetParent.className == 'player two'  && 
      this.checkIfNumberIsHigerThanHundrad(event.key, this.player2)){
        return false;
      }
    }
    return true;
  }

  checkIfNumberIsHigerThanHundrad(num, play){
    if (isNaN(play))
      return false;
    if (play + num > 100)
      return true;
    return false
  }

  getLowestNumerFromRandomNumber(number: number): number{
    if (number >= this.randomNumer) {
      return (number - this.randomNumer);
    }
    return this.randomNumer - number;
  }

  determTheWinner() : string{
    var p1 = this.getLowestNumerFromRandomNumber(this.player1);
    var p2 = this.getLowestNumerFromRandomNumber(this.player2);
    
    if (p1 > p2) {
      return 'Player 2 is the lucky one';
    }
    else if (p1 < p2) {
      return 'Player 1 is the lucky one';
    } 
    else {
      return 'Both are the winner';
    }
  }
}
