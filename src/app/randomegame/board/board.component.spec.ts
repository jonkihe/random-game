import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardComponent } from './board.component';

describe('BoardComponent', () => {
  let component: BoardComponent;
  let fixture: ComponentFixture<BoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('checkIfNumberIsHigerThanHundrad', () =>{
    it('should check if player is a NaN and return false', () => {
      expect(component.checkIfNumberIsHigerThanHundrad(1, NaN)).toEqual(false);
    })
    it('If number is higer then 100 should retun true', () => {
      expect(component.checkIfNumberIsHigerThanHundrad('1',100)).toEqual(true);
      expect(component.checkIfNumberIsHigerThanHundrad('0',100)).toEqual(true);
      expect(component.checkIfNumberIsHigerThanHundrad('1',10)).toEqual(true);
      expect(component.checkIfNumberIsHigerThanHundrad('9',99)).toEqual(true);
      expect(component.checkIfNumberIsHigerThanHundrad(0,100)).toEqual(false);
    })
  });

  describe('getLowestNumerFromRandomNumber', () => {
    it('compair outcome with randomenumber',() =>{
      component.randomNumer = 7;
      expect(component.getLowestNumerFromRandomNumber(7)).toEqual(0);
      expect(component.getLowestNumerFromRandomNumber(5)).toEqual(2);
      expect(component.getLowestNumerFromRandomNumber(9)).toEqual(2);
    })
  });

  describe('determTheWinner', () =>{
    it('player 1 should be the winner', () => {
      component.randomNumer = 50;
      component.player2 = 55;
      component.player1 = 46;
      expect(component.determTheWinner()).toEqual('Player 1 is the lucky one');
      component.player2 = 45; 
      expect(component.determTheWinner()).toEqual('Player 1 is the lucky one');
    })
    it('boath player should be the winners', () => {
      component.randomNumer = 50;
      component.player2 = 55;
      component.player1 = 45;
      expect(component.determTheWinner()).toEqual('Both are the winner');
      component.player2 = 45;
      expect(component.determTheWinner()).toEqual('Both are the winner');
      
    });

  });
});
